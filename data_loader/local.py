import csv
import logging
import math
from datetime import (datetime,
                      timedelta
                      )

import pandas as pd

from core.config import (DataConfig,
                         ModuleConfig
                         )
from data_loader.columns import (BILLING,
                                 CRM,
                                 API_respone
                                 )

logger = logging.getLogger(ModuleConfig.MODULE_NAME)

HEADER_COLUMNS = [CRM.customer_id, CRM.customer_name, CRM.customer_age]
PHONE_NUMBER_HEADER_COLUMNS = [CRM.customer_phone_number, CRM.customer_sim, CRM.activation_date, CRM.deactivation_date]


class Loader:
    def _save_df(self, df: pd.DataFrame, file_name: str):
        df.to_csv(str(file_name), index=False, encoding="utf-8-sig", quoting=csv.QUOTE_NONNUMERIC)

    def _load_df(self, file_name: str) -> pd.DataFrame:
        return pd.read_csv(str(file_name), keep_default_na=False, encoding="utf-8-sig", dtype=str)

    def __init__(self):
        logger.debug(f"Loading input files from source:"
                     f"\nBilling: {DataConfig.BILING_PATH.absolute()}"
                     f"\nCRM: {DataConfig.CRM_PATH.absolute()}")

        self.bill_df = self._load_df(DataConfig.BILING_PATH)
        self.bill_df[BILLING.customer_phone_number] = self.bill_df[BILLING.customer_phone_number].astype(str)
        self.bill_df[BILLING.call_start] = pd.to_datetime(self.bill_df[BILLING.call_start])
        self.bill_df[BILLING.call_duration] = self.bill_df[BILLING.call_duration].astype(int)
        self.bill_df[BILLING.call_duration_minutes] = self.bill_df[BILLING.call_duration].apply(
            lambda x: math.ceil(x / 60)
        )
        self.bill_df[BILLING.call_duration] = self.bill_df[BILLING.call_duration].astype(str)

        self.crm_df = self._load_df(DataConfig.CRM_PATH)
        self.crm_df.loc[self.crm_df[CRM.deactivation_date] == 'NULL', CRM.deactivation_date] = '2022-12-31 23:59:59'

    def _find_relevant_calls(self,
                             customer_phone_number: str,
                             active_from: datetime,
                             active_to: datetime) -> pd.DataFrame:
        result_df = self.bill_df.loc[self.bill_df[BILLING.customer_phone_number] == customer_phone_number]
        result_df = result_df.loc[result_df[BILLING.call_start] <= active_to]
        result_df = result_df.loc[result_df[BILLING.call_start] >= active_from]
        result_df[BILLING.call_start] = result_df[BILLING.call_start].astype(str)

        return result_df

    def _find_customer(self, customer_id: str, ):
        return self.crm_df.loc[self.crm_df[CRM.customer_id] == customer_id]

    def customer_calls_for_past_3_days(self, customer_id: str, current_date: datetime, time_dif=timedelta(hours=72)):
        result_json = {}
        phone_numbers = {}

        customer_df = self._find_customer(customer_id)

        for header_column in HEADER_COLUMNS:
            if (customer_df[header_column] == customer_df[header_column].values[0]).all():
                result_json[header_column] = customer_df[header_column].values[0]
            else:
                ERROR_MESSAGE = f"The data of columns: {header_column} " \
                                f"for id: {customer_id} is not same for all records"
                logger.error(ERROR_MESSAGE)
                return ERROR_MESSAGE

        for index, row in customer_df.iterrows():
            phone_number_result = {}
            for phone_header_column in PHONE_NUMBER_HEADER_COLUMNS:
                phone_number_result[phone_header_column] = row[phone_header_column]

            activation_date = datetime.fromisoformat(row[CRM.activation_date])
            past_time = current_date - time_dif

            datetime_to_start = max(activation_date, past_time)

            result_df = self._find_relevant_calls(customer_phone_number=row[CRM.customer_phone_number],
                                                  active_from=datetime_to_start,
                                                  active_to=current_date
                                                  )

            phone_number_result[API_respone.phone_calls] = result_df.to_dict('records')
            phone_numbers[row[CRM.customer_phone_number]] = phone_number_result

        result_json[API_respone.phone_numbers] = phone_numbers

        return result_json

    def get_all_customers_id(self) -> list:
        return list(self.crm_df[CRM.customer_id].unique())

    def _find_all_calls_for_customer(self, customer_id: str) -> pd.DataFrame:
        phone_call_df = pd.DataFrame()

        customer_df = self._find_customer(customer_id)
        for index, row in customer_df.iterrows():
            temp_call_df = self._find_relevant_calls(row[CRM.customer_phone_number],
                                                     row[CRM.activation_date],
                                                     row[CRM.deactivation_date]
                                                     )

            phone_call_df = pd.concat([phone_call_df, temp_call_df])

        return phone_call_df

    def get_all_calls_for_bar(self, customer_list: list) -> pd.DataFrame:
        result_df = pd.DataFrame()
        for customer in customer_list:
            temp_customer_df = self._find_all_calls_for_customer(customer)
            temp_customer_df[CRM.customer_id] = customer
            result_df = pd.concat([result_df, temp_customer_df])

        result_df[BILLING.call_duration] = result_df[BILLING.call_duration].astype(int)
        result_df = result_df.rename(columns={BILLING.customer_phone_number: CRM.customer_phone_number})

        return result_df
