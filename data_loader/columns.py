class BILLING:
    customer_phone_number = 'A_party'
    target_phone_number = 'B_party'
    call_duration = 'call_duration'
    call_price = 'call_price'
    call_start = 'call_start'
    call_duration_minutes = 'call_duration_minutes'


class CRM:
    customer_id = 'customer_id'
    customer_phone_number = 'customer_phone'
    activation_date = 'activation_date'
    deactivation_date = 'deactivation_date'
    customer_sim = 'customer_sim'
    customer_name = 'customer_name'
    customer_age = 'customer_age'


class API_respone:
    phone_calls = 'phone_calls'
    phone_numbers = 'phone_numbers'
