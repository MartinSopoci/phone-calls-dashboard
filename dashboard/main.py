import streamlit as st

from dashboard.charts import altair_chart
from dashboard.selector_components import customer_filter


def run_dashboard():
    st.set_page_config(layout="wide")

    customer_filter()

    altair_chart()
