import streamlit as st

from dashboard.stream_setup import LABEL
from data_loader import PHONE_LOADER


def customer_filter():
    customer_ids = PHONE_LOADER.get_all_customers_id()
    multi_container = st.container()

    st.session_state.selected_all_customer_ids = multi_container.checkbox(label=LABEL.checkbox,
                                                                          value=True
                                                                          )
    if st.session_state.selected_all_customer_ids:
        st.session_state.selected_customer_ids = customer_ids
    else:
        st.session_state.selected_customer_ids = multi_container.multiselect(label=LABEL.filter, options=customer_ids)
