import streamlit as st


class VARIABLES:
    selected_customer_ids = 'selected_customer_ids'
    selected_all_customer_ids = 'selected_all_customer_ids'


if VARIABLES.selected_customer_ids not in st.session_state:
    st.session_state[VARIABLES.selected_customer_ids] = []

if VARIABLES.selected_all_customer_ids not in st.session_state:
    st.session_state[VARIABLES.selected_all_customer_ids] = True


class LABEL:
    filter = 'Filter by customer ID'
    checkbox = 'Select all customers'
