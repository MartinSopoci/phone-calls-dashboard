import altair as alt
import streamlit as st

from data_loader import PHONE_LOADER
from data_loader.columns import CRM, BILLING


def altair_chart():
    st.info
    if len(st.session_state.selected_customer_ids) > 0:
        df = PHONE_LOADER.get_all_calls_for_bar(st.session_state.selected_customer_ids)

        chart = (
            alt.Chart(df).properties(height=700)
                .mark_bar()
                .encode(
                alt.X(BILLING.call_start),
                alt.Y(BILLING.call_duration_minutes),
                alt.Color(CRM.customer_phone_number),
                alt.Tooltip([BILLING.call_start,
                             BILLING.call_duration_minutes,

                             CRM.customer_phone_number,
                             CRM.customer_id,
                             BILLING.target_phone_number,
                             BILLING.call_price])
                ,
            )
                .interactive()

        )
        st.altair_chart(chart, use_container_width=True)
