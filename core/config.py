from pathlib import Path


class ModuleConfig:
    MODULE_NAME = 'phone_calls_dashboard'


class LoggConfig:
    LOG_FILE_PATH = Path(f"./{ModuleConfig.MODULE_NAME}.log")


class APIConfig:
    HOST = '0.0.0.0'  # '127.0.0.1'  #
    PORT = 8000


class DataConfig:
    BILING_PATH = Path('./input_data/billing_random.csv')
    CRM_PATH = Path('./input_data/crm_random.csv')
