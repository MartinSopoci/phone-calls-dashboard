import logging
from datetime import datetime

import uvicorn
from fastapi import FastAPI

from core.config import (ModuleConfig,
                         APIConfig
                         )
from data_loader import PHONE_LOADER

logger = logging.getLogger(ModuleConfig.MODULE_NAME)

tags_metadata = [
    {
        "name": "customer_calls",
        "description": "Returns list of his calls for last 72 hours."
    },
]

app = FastAPI(openapi_tags=tags_metadata)



@app.get("/customer_calls", tags=["customer_calls"])
async def extract_document_lines(customer_id: str = 'c_58',
                                 current_date: datetime = datetime.fromisoformat('2022-12-28 00:00:00')
                                 ):
    return PHONE_LOADER.customer_calls_for_past_3_days(customer_id, current_date)


def run_api():
    logger.debug(f"Started API on: {APIConfig.HOST}:{APIConfig.PORT}")
    uvicorn.run(app, host=APIConfig.HOST, port=APIConfig.PORT)
