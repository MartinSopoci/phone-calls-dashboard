FROM python:3.9

WORKDIR /hone-calls-dashboard

COPY . .

RUN pip install -r requirement.txt

EXPOSE 8000
EXPOSE 5000

CMD ["./run.sh"]
