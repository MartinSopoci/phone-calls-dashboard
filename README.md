# Phone calls dashboard

In this project you can find solution for case study. In folder called solution there is SQL command to request result and also power BI dashboard. Rest of the repository contains solution for running API. It's using python 3.9

### Start:
install requirements.txt \
execute main.py

or:  

docker build -t phone-calls-dash . \
docker run -p 5000:5000 -p 8000:8000 phone-calls-dash

### Main API url:

http://127.0.0.1:8000/docs

### API usage:
You need to provide customer ID. The customer ids range from c_1 to c_113. All data is randomly generated stored in folder input_data. All data is situated in year 2022, so you need to specify current date to count 72 hours to past.

### Main Dashboard url:

http://localhost:5000/

#### TODO:
* implement front end dashboard.

### More of my projects can be found on GitLab:

https://gitlab.com/MartinSopoci