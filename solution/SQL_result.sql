
WITH SUMMARY_TABLE as (
SELECT 
CRM.crm_id, CRM.customer_id, CRM.customer_phone, CRM.customer_sim, CRM.activation_date,  IFNULL(CRM.deactivation_date, CURRENT_TIMESTAMP()) as "deactivation_date", CRM.customer_name, CRM.customer_age,
BIL.bil_id, BIL.A_party, BIL.B_party, BIL.call_duration, BIL.call_price, BIL.call_start,
date(BIL.call_start) as "call_day"
FROM telekom.CRM as CRM
LEFT JOIN telekom.Billing BIL
ON CRM.customer_phone = BIL.A_party
WHERE date(BIL.call_start) between date(CRM.activation_date) and date(IFNULL(CRM.deactivation_date, CURRENT_TIMESTAMP()))
)

SELECT call_day, customer_id, SUM(call_duration) as "seconds_of_call", SUM(call_duration)/60 as "minutes_of_call"

FROM SUMMARY_TABLE
GROUP by call_day, customer_id
ORDER by customer_id;



